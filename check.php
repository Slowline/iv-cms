<?php

require 'inc/common.php';

foreach( $db->update_file as $file )
	if( !is_file( $file['path'] )) {
		echo "File not found: {$file['path']} in package {$file['package']}\n";
		if( !empty( $argv[1] )) $db->update_file->del("path = '%s'", $file['path'] );
	} else {
		if( $file['hash'] != md5_file( $file['path'] )) {
			echo "Hash missmatch for {$file['path']} in package {$file['package']}\n";
		}

		if( base64_decode($file['content']) != file_get_contents( $file['path'] )) {
			echo "Content missmatch for {$file['path']} in package {$file['package']}\n";
			if( !empty( $argv[1] )) $db->update_file->updateRow( array('content' => base64_encode(file_get_contents( $file['path'] ))), $file['path'], 'path' );
		}
	}
