<?php

$forumView = new forum_view();

if( !empty( $_GET['moderate'])) {
	$thread = forum_thread::load( $_GET['moderate'] );
	$board = $thread->loadBoard();
	$board->checkRights( 'moderate' );

	if(isset($_GET['delete'])) {
		$thread->delete();
		throw new redirect(PAGE_SELF.'&board='.$board->id);
	} elseif(isset($_GET['close']) || isset($_GET['reopen'])) {
		$thread->close(isset($_GET['close']));
	} elseif(!empty($_GET['move'])) {
		$destination = forum_board::load($_GET['move']);
		$thread->move($destination);
	} elseif(!empty($_POST['rename'])) {
		$thread->rename($_POST['rename']);
	}

	throw new redirect(PAGE_SELF.'&thread='.$thread->id);
} elseif( !empty($_GET['delete'])) {
	if( !$posting = db()->forum_post->row($_GET['delete'])->assoc())
		throw new Exception('Posting not found');
	$thread = forum_thread::load( $posting['thread'] );
	$board = $thread->loadBoard();

	if( $user->id != $posting['create_by'] || !$user )
		$board->checkRights( 'moderate' );

	db()->forum_post->delRow( $posting['id'] );
	$thread->update();
	throw new redirect(PAGE_SELF.'&thread='.$thread->id);
} elseif( !empty($_GET['edit'])) {
	if( !$posting = db()->forum_post->row($_GET['edit'])->assoc())
		throw new Exception('Posting not found');
	$thread = forum_thread::load( $posting['thread'] );
	$board = $thread->loadBoard();

	if( $user->id != $posting['create_by'] || !$user )
		$board->checkRights( 'moderate' );

	if( !empty( $_POST['title']) && !empty( $_POST['text'])) {
		db()->forum_post->updateRow(array(
				'title'=>$_POST['title'],
				'text_raw'=>$_POST['text']
		), $posting['id']);
		throw new redirect(PAGE_SELF.'&thread='.$thread->id);
	}

	$forumView->edit($posting, $thread);
} elseif( !empty($_GET['create'])) {
	$board = forum_board::load($_GET['create']);
	$board->checkRights( 'write' );

	if( !empty( $_POST['title']) && !empty( $_POST['text'])) {
		$threadId = $board->write( $_POST['title'], $_POST['text'] );
		throw new redirect(PAGE_SELF.'&thread='.$threadId);
	}

	$forumView->createThread($board);
} elseif( !empty( $_GET['reply'])) {
	$thread = forum_thread::load( $_GET['reply'] );
	$board = $thread->loadBoard();
	$board->checkRights( 'reply' );

	if( $thread->closed ) $board->checkRights('moderate');
	if( !empty( $_POST['title']) && !empty( $_POST['text'])) {
		$thread->reply( $_POST['title'], $_POST['text'] );
		throw new redirect(PAGE_SELF.'&thread='.$thread->id);
	}

	$forumView->reply($thread);
} elseif( !empty( $_GET['thread'])) {
	$forumView->showThread(db()->forum_thread->row( $_GET['thread'] )->object( 'forum_thread' ));
} elseif( !empty( $_GET['board'])) {
	$forumView->showBoard( $_GET['board'] );
} else {
	$forumView->listBoards();
}