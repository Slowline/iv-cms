<?php

function install() {
	db()->query("ALTER TABLE `forum_board`
		ADD `count_posts` INT UNSIGNED NOT NULL AFTER `last_post` ,
		ADD `count_threads` INT UNSIGNED NOT NULL AFTER `count_posts`,
		ADD `public_reply` BOOLEAN NOT NULL AFTER `public_write`;");
}

function remove() {
	db()->query("");
}
