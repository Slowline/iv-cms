<?php

/**
 * Class mediaManager
 */
class mediaManager extends controller {

	const UPLOAD_DIR = 'upload/';

	public function index() {
		global $conf;

		$categoryId = intval( $_GET['category'] );
		$self = MODUL_SELF.'&category='.$categoryId;

		// upload files
		if( isset( $_FILES['file'] ) && isset( $_POST['category'] )) {
			$file = $_FILES['file'];
			$category = intval( $_POST['category'] );

			if( !$file['name'] && !$file['tmp_name'] ) {
				$this->view->error( 'Es muss eine Datei für den Upload ausgewählt werden!' );
			} elseif( !$category ) {
				$this->view->error( 'Es muss eine Kategorie für den Upload ausgewählt werden!' );
			} else {
				// May be restricted by modul rights later
				$types = array('png', 'jpg', 'jpeg', 'gif');

				$fileClass = new upload_attachment($file);
				$fileClass->restrictFileSize( 1024 * 1024 * 2 )->restrictType( $types );
				$fileClass->attach( 'media', $category );
				$fileClass->save( self::UPLOAD_DIR.'upload_'.uniqid() );
				$this->view->success( 'Upload erfolgreich!' );
			}
		}

		// move files
		if( isset( $_GET['move'] ) && $categoryId ) {
			upload_list::move( $_GET['move'], $categoryId );
			throw new redirect( $self );
		}

		// delete file hook
		if( isset( $_GET['deleteImg'] ) ) {
			$img = db()->t('content_upload')->row( (int) $_GET['deleteImg'] )->assoc();
			upload_list::delete( $img['id'] );
			$writer = new writer_fs();
			$writer->delete( $img['path'] );
			$this->view->success( 'Datei erfolgreich gelöscht!' );
		}

		// category data controller
		$rc =  new data_controller( 'content_upload_category', $self );
		$rc->add( 'id', 'ID', 1, 0, 0, 0 );
		$rc->add( 'name', 'Name' );

		// edit category form
		if( $_GET['edit'] ) {
			$this->view->content( $rc->get_edit( $_GET['edit'] ));
			$this->view->format = 'plain';
			return;
		}

		// delete hook
		if( isset( $_GET['delete'] ) ) {
			$list = new upload_list( 'media' );

			if( $list->get($_GET['delete'])->num_rows()) {
				$this->view->error('Die Kategorie kann nicht gelöscht werden, da sie noch Dateien enthält!');
				$run = false;
			} else $run = true;
		} else $run = true;

		// run category actions
		if( $run && $rc->run()) throw new redirect( $self );

		// upload form
		$uploadForm = new form_renderer( $self, 'Hochladen' );
		$uploadForm->select( 'category', 'Kategorie', array( 0 => "Bitte Wählen" ) + db()->t('content_upload_category')->get(1)->relate(), $categoryId);
		$uploadForm->upload( 'file', 'Datei' );

		$error = false;
		$data = array();

		// filter files by category
		if( $_GET['category'] > 0 ) {
			$list = new upload_list( 'media' );
			$data = $list->get($categoryId);

			if( !$data->num_rows() )
				$error = 'Keine Ergebnisse!';
		} else $error = "Keine Ergebnisse!";

		// render the page
		$this->view->content( template('iv.media')->render(array(
			'categories' => db()->t('content_upload_category')->all(),
			'modul_self' => MODUL_SELF,
			'current' => $categoryId,
			'images' => $data,
			'url' => trim($conf->page->url, '/'),
			'error' => $error,
			'category' => $categoryId,
			'uploadForm' => $uploadForm,
			'createForm' => $rc->get_create(),
		)));
	}
}

// Start the media manager
new mediaManager( $view );
