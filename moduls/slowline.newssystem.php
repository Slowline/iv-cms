<?php
class newsSystem extends controller {
	public function index() {
		$categoryId = intval ( $_GET ['category'] );
		$self = MODUL_SELF . '&category=' . $categoryId;
		
		if (! empty ( $_POST )) {
			// Name Validation
			if (isset ( $_POST ['news_title'] ) && isset ( $_POST ['news_short_description'] ) && isset ( $_POST ['news_description'] ) && isset ( $_POST ['news_content'] )) {
				$db->query ( "INSERT INTO news_articles ( `title`, `short_description`, `description`, `content`, `created_by`, `created_date`, `content` ) %d, %s, %f, %e, id, SELECT UNIX_TIMESTAMP(), %g", $_POST ['news_title'], $_POST ['news_short_description'], $_POST ['news_description'], $_POST ['news_content'], $categoryId );
				throw new redirect ( MODUL_SELF . '&success' );
			}
		}
		
		// category data controller
		$rc = new data_controller ( 'news_category', $self );
		$rc->add ( 'id', 'ID', 1, 0, 0, 0 );
		$rc->add ( 'name', 'Name' );
		
		// edit category form
		if ($_GET ['edit']) {
			$this->view->content ( $rc->get_edit ( $_GET ['edit'] ) );
			$this->view->format = 'plain';
			return;
		}
		
		// delete hook
		if (isset ( $_GET ['delete'] )) {
			$list = new upload_list ( 'media' );
			
			if ($list->get ( $_GET ['delete'] )->num_rows ()) {
				$this->view->error ( 'Die Kategorie kann nicht gelöscht werden, da sie noch Dateien enthält!' );
				$run = false;
			} else
				$run = true;
		} else
			$run = true;
			
			// run category actions
		if ($run && $rc->run ())
			throw new redirect ( $self );
		
		if (isset ( $_GET ['success'] ))
			$view->box ( '<p alifn="center">Artikel erfolgreich erstellt!</p>', 'Artikel erfolgreich erstellt' );
			
			// Create Form for Article Creation
		$form = new form ( MODUL_SELF, 'Artikel verfassen' );
		$form->text ( 'news_title', 'Titel' )->input ( 'class', 'input-xxlarge' );
		$form->text ( 'news_short_description', 'Kurzbeschreibung' )->input ( 'class', 'input-xxlarge' );
		
		$form->text ( 'news_description', 'Beschreibung' )->input ( 'class', 'input-xxlarge' );
		$form->textarea ( 'news_content', 'Inhalt' )->input ( 'class', '' )->input ( 'rows', 10 )->input ( 'class', 'input-xxlarge' );
		
		$error = false;
		
		// render the page
		$this->view->content ( template ( 'slowline.newssystem' )->render ( array (
				'categories' => db ()->t ( 'news_category' )->all (),
				'modul_self' => MODUL_SELF,
				'current' => $categoryId,
				'currentCategoryString' => db ()->query ( "SELECT name FROM news_category WHERE id = %d", $categoryId )->value (),
				'error' => $error,
				'category' => $categoryId,
				'createArticle' => $form,
				'createForm' => $rc->get_create () 
		) ) );
	}
}

// Start the media manager
new newsSystem ( $view );