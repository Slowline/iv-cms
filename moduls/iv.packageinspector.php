<style>
	code { background: none; border: none; width: 100%; overflow: auto; display: block; }
</style><?php


if( empty( $_GET['server'] )) {
	$form = new form( IV_SELF, 'Pekete auflisten', 'get' );
	$form->hidden('modul', 'iv.packageinspector');
	$form->text('server', 'Server-URL');
	$view->box( $form, 'Paketliste abrufen');
} elseif( empty( $_GET['package'] )) {
	$url = $_GET['server'].'?interface=iv.exchange&plain';
	if( ! $packages = json_decode(file_get_contents( $url ), true))
		throw new Exception('Invalid Server');

	$list = new list_array( $link = MODUL_SELF.'&server='.$_GET['server'] );
	$list->text('Name', 'name');
	$list->text('Autor', 'autor');
	$list->text('Beschreibung', 'description');
	$list->add( new list_column_actions('Aktionen'))->add( $link, 'package', 'Anzeigen', 'assets/small/camera.png' );

	$view->box( $list->get($packages), 'Paketliste');
} else {
	$url = $_GET['server'].'?interface=iv.exchange&package='.$_GET['package'].'&plain';
	if( !$package = json_decode(file_get_contents( $url ), true))
		throw new Exception('Invalid Package');

	foreach( $package['files'] as $file )
		$view->box( highlight_string( base64_decode( $file['content'] ), true ), $file['path'] );
}
