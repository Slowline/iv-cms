# README #

Dies ist die fünfte Version des IV-CMS, einem System was nicht direkt für Endanwender gedacht ist, sondern mehr für Entwickler und Programmieranfänger. 

### Reguläre Installation ###

Diese Option steht leider noch nicht zur Verfügung.

### Installation aus dem Repository ###

Um das System zum laufen zu bringen, muss man nach dem checkout noch folgende schritte befolgen:

1. die Datei inc/mysql.example.php kopieren, mit den Zugangsdaten der DB versehen und in inc/mysql.config.php umbenennen
2. die setup.php einmal in der Konsole ausführen
3. über phpMyAdmin oder ein ähnliches Werkzeug einen User anlegen, das Passwort muss bei leerem salt doppelt mit md5 verschlüsselt werden. Der Type sollte wie beim alten System 7 entsprechen
4. einen Ordner "cache" erstellen mit Rechten 777

Wer über keinen SSH-Zugang verfügt findet zu Schritt 2 hier noch ein paar tipps:
http://iv-cms.de/Forum?&thread=55
