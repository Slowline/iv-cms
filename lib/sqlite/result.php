<?php

class sqlite_result implements Iterator {
	/** @var \SQLiteResult  */
	protected $res;

	public function  __construct( SQLiteResult $res ) {
		$this->res = $res;
	}

	/**
	 * Returns the number of rows found
	 * @return int
	 */
	public function num_rows() {
		return $this->res->numRows();
	}

	/**
	 * Fetches a record as associative array
	 * @return array
	 */
	public function assoc() {
		return $this->res->fetch( SQLITE_ASSOC );
	}

	/**
	 * Fetches a record as numeric array
	 * @return array
	 */
	public function row() {
		return $this->res->fetch( SQLITE_NUM );
	}

	/**
	 * Fetches a record as object
	 * @return stdclass
	 */
	public function object() {
		return $this->res->fetchObject();
	}

	/**
	 * Fetches a singe value
	 * @param int $key column number
	 * @return mixed
	 */
	public function value( $key = 0 ) {
		$erg = $this->row();
		return $erg[$key];
	}

	/**
	 * Fetch all records in two dimensional accociative array
	 * @param string $key
	 * @return array
	 */
	public function assocs( $key = NULL ) {
		$erg = array();

		if( $key ) while( $row = $this->assoc()) $erg[$row[$key]] = $row;
		else while( $row = $this->assoc()) $erg[] = $row;

		return $erg;
	}


	/**
	 * Fetch all records in two dimensional numeric array
	 * @param int $key
	 * @return array
	 */
	public function rows( $key = NULL ) {
		$erg = array();

		if( $key ) while( $row = $this->row()) $erg[$row[$key]] = $row;
		else while( $row = $this->row()) $erg[] = $row;

		return $erg;
	}


	/**
	 * Fetch all records in array of objects
	 * @param string $key
	 * @return array
	 */
	public function objects( $key = NULL ) {
		$erg = array();

		if( $key ) while( $row = $this->object()) $erg[$row->{$key}] = $row;
		else while( $row = $this->object()) $erg[] = $row;

		return $erg;
	}

	public function values( $key = 0 ) {
		$erg = array();

		while( $row = $this->row())
			$erg[] = $row[$key];

		return $erg;
	}

	public function relate( $value = 'name', $key = 'id' ) {
		$erg = array();

		while( $row = $this->assoc())
			$erg[$row[$key]] = $row[$value];

		return $erg;
	}

	public function current() {
		return $this->res->current();
	}

	public function key() {
		return $this->res->key();
	}

	public function next() {
		$this->res->next();
	}

	public function rewind() {
		$this->res->rewind();
	}

	public function valid() {
		return $this->res->valid();
	}
}
